#ifndef STATS_H
#define STATS_H

namespace typedeph {
  
  namespace stats {
    
    template <typename T>
    double mean(T coll)
    {
      int sum = 0;
      for (auto el : coll)
        sum += el;

      return sum / coll.size();
    }

  }
  
}

#endif
