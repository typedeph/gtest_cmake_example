#include <iostream>
#include <vector>
#include "stats.hpp"

int main()
{
  std::cout << typedeph::stats::mean<std::vector<int>>({1, 2, 3, 4})
            << std::endl;
}
