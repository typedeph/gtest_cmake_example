My version of using cmake with a cpp project structure using gtest + gtest_main.
Mostly taken from [kaizouman](https://github.com/kaizouman/gtest-cmake-example).
He has a good, detailed explanation here: 

## Other Potential Learning CMake Resources
* [Crascit](https://github.com/Crascit/DownloadProject)
  * His website explaning the cmake tool/module:
    <https://crascit.com/2015/07/25/cmake-gtest/>
* [Video on cpp dependency management](https://youtu.be/UnHeOywnj1k)
